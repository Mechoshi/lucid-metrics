const { BaseHrefWebpackPlugin } = require('base-href-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const { spawn } = require('child_process');
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');

const cssResourcesPath = require(path.join(
  __dirname,
  'src',
  'renderer',
  'styles',
  'shared',
  'index.ts'
));

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 8080;

const devName = 'public/[name]';
const prodName = 'public/[name].[chunkhash]';

module.exports = (env, argv) => {
  const isDev = argv.mode === 'development';

  const config = {
    entry: {
      app: './src/renderer/index.ts'
    },
    output: {
      filename: `${isDev ? devName : prodName}.js`,
      chunkFilename: `${isDev ? devName : prodName}.js`,
      path: path.resolve(__dirname, 'dist'),
      publicPath: isDev ? '/' : './'
    },
    resolve: {
      alias: {
        '@components': path.resolve(__dirname, 'src/renderer/components'),
        '@pages': path.resolve(__dirname, 'src/renderer/pages'),
        '@providers': path.resolve(__dirname, 'src/renderer/providers'),
        '@services': path.resolve(__dirname, 'src/renderer/services')
      },
      extensions: ['.js', '.jsx', '.ts', '.tsx']
    },
    module: {
      rules: [
        {
          test: /\.worker\./,
          use: { loader: 'worker-loader' }
        },
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              plugins: [isDev && require.resolve('react-refresh/babel')].filter(
                Boolean
              )
            }
          }
        },
        {
          test: /\.(woff|woff2|ttf|eot)$/,
          type: 'asset/resource',
          generator: {
            filename: 'public/fonts/[name].[ext]'
          }
        },
        {
          test: /\.(png|jpe?g|gif|svg|ico)$/,
          type: 'asset/resource',
          generator: {
            filename: 'public/images/[name].[ext]'
          }
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader
            },
            {
              loader: 'css-loader',
              options: {
                importLoaders: 3,
                modules: {
                  localIdentName: '[name]__[local]__[hash:base64:5]'
                },
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  plugins: ['autoprefixer']
                }
              }
            },
            {
              loader: 'sass-loader'
            },
            {
              loader: 'sass-resources-loader',
              options: {
                resources: cssResourcesPath
              }
            }
          ]
        }
      ]
    },
    plugins: [
      new BaseHrefWebpackPlugin({ baseHref: isDev ? '/' : './' }),
      new ForkTsCheckerWebpackPlugin({
        typescript: {
          configFile: './src/renderer/tsconfig.json'
        }
      }),
      new CleanWebpackPlugin({
        cleanOnceBeforeBuildPatterns: ['dist', isDev && '!main.js'].filter(
          Boolean
        )
      }),
      new HtmlWebPackPlugin({
        template: './src/renderer/index.html'
      }),
      new MiniCssExtractPlugin({
        filename: `${isDev ? devName : prodName}.css`,
        chunkFilename: `${isDev ? devName : prodName}.css`
      }),
      new webpack.DefinePlugin({
        __MODE__: JSON.stringify(argv.mode)
      }),

      !isDev && new OptimizeCSSAssetsPlugin(),
      !isDev && new TerserPlugin(),
      isDev && new ReactRefreshWebpackPlugin()
    ].filter(Boolean),
    devtool: isDev ? 'eval-source-map' : 'source-map',
    devServer: {
      onListening() {
        const environment = Object.create(process.env);

        environment.ELECTRON_DISABLE_SECURITY_WARNINGS = false;
        environment.HOST = host;
        environment.NODE_ENV = argv.mode;
        environment.PORT = port;

        spawn('electron', ['.'], {
          shell: true,
          env: environment,
          stdio: 'inherit'
        })
          .on('close', () => process.exit(0))
          .on('error', (spawnError) => console.error(spawnError));
      },
      client: {
        logging: 'error',
        overlay: true
      },
      compress: true,
      historyApiFallback: {
        index: 'http://localhost:8080'
      },
      host,
      hot: true,
      port
    }
  };

  return config;
};
