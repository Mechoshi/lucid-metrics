const { resolve } = require("path");

module.exports = (env, argv) => {
  const isDev = argv.mode === "development";

  const config = {
    mode: argv.mode,
    entry: "./src/main/index.ts",
    output: {
      filename: "main.js",
      path: resolve(__dirname, "dist"),
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          include: /src\/main/,
          use: [{ loader: "ts-loader" }],
        },
      ],
    },
    target: "electron-main",
  };

  return config;
};
