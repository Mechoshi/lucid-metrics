import { app, BrowserWindow, session } from 'electron';
import { homedir } from 'os';
import { join, resolve } from 'path';

const env = process.env.NODE_ENV || 'development';
const isDev = env === 'development';

const createWindow = () => {
  const win = new BrowserWindow({
    width: 1280,
    height: 1024,
    webPreferences: {
      devTools: isDev
    }
  });

  if (isDev) {
    win.loadURL(`http://${process.env.HOST}:${process.env.PORT}`);

    win.webContents.openDevTools();
  } else {
    win.loadFile(resolve(__dirname, '..', 'dist', 'index.html'));
  }

  return win;
};

app.whenReady().then(() => {
  createWindow();

  // (macOS): Don't open new window
  // if already running:
  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });

  if (isDev) {
    const reactDevToolsID = 'fmkadmapgofadopljbjfkapdkoienihi';
    const reactDevToolsVersion = '4.18.0';
    const reactDevToolsFolder = '.config/google-chrome/Default/Extensions/';

    const reactDevToolsPath = join(
      homedir(),
      `${reactDevToolsFolder}${reactDevToolsID}/${reactDevToolsVersion}_0`
    );

    session.defaultSession.loadExtension(reactDevToolsPath);
  }
});

// (Windows & Linux): Quit the app
// when all windows are closed:
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
