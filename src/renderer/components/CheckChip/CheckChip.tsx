import React, { ChangeEvent, FC, memo } from 'react';

import { classer } from '@services';

import styles from './CheckChip.scss';

interface ICheckChipProps {
  checked: boolean;
  className?: string;
  disabled?: boolean;
  label: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

const CheckChip: FC<ICheckChipProps> = ({
  checked,
  className,
  disabled,
  label,
  onChange
}) => {
  return (
    <label className={classer([styles.Container, className])}>
      <input
        checked={checked}
        className={styles.Input}
        disabled={disabled}
        onChange={onChange}
        type="checkbox"
      />
      <span className={styles.Label}>{label}</span>
    </label>
  );
};

export default memo(CheckChip);
