/* eslint-disable max-len */
import React, { FC, memo, useMemo } from 'react';

import styles from './Loader.scss';

interface ILoaderProps {
  width?: number; // pixels
}

const Loader: FC<ILoaderProps> = ({ width = 100 }) => {
  const height = useMemo(() => Math.round((width * 2) / 3), [width]);

  return (
    <svg
      className={styles.Container}
      height={`${height}px`}
      preserveAspectRatio="xMidYMid meet"
      viewBox="0 0 187.3 93.7"
      width={`${width}px`}
    >
      <path
        className={styles.AnimatedPath}
        d="M93.9,46.4c9.3,9.5,13.8,17.9,23.5,17.9s17.5-7.8,17.5-17.5s-7.8-17.6-17.5-17.5c-9.7,0.1-13.3,7.2-22.1,17.1 
        c-8.9,8.8-15.7,17.9-25.4,17.9s-17.5-7.8-17.5-17.5s7.8-17.5,17.5-17.5S86.2,38.6,93.9,46.4z"
      />
      <path
        className={styles.BackgroundPath}
        d="M93.9,46.4c9.3,9.5,13.8,17.9,23.5,17.9s17.5-7.8,17.5-17.5s-7.8-17.6-17.5-17.5c-9.7,0.1-13.3,7.2-22.1,17.1 
        c-8.9,8.8-15.7,17.9-25.4,17.9s-17.5-7.8-17.5-17.5s7.8-17.5,17.5-17.5S86.2,38.6,93.9,46.4z"
      />
    </svg>
  );
};

export default memo(Loader);
