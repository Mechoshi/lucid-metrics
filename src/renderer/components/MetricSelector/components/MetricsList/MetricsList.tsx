import React, { FC, memo, useMemo } from 'react';

import { CheckChip } from '@components';
import { useStatus } from '@providers';
import {
  classer,
  FILESPACE_CONNECTION_STATUSES,
  MetricLog,
  MetricsCategoryLog
} from '@services';

import styles from './MetricsList.scss';

interface IMetricsListProps {
  canAddMetric: boolean;
  categoryLog: MetricsCategoryLog;
  className?: string;
  selected: MetricLog[];
  updateSelected: (metricLog: MetricLog) => void;
}

const MetricCategory: FC<IMetricsListProps> = ({
  canAddMetric,
  categoryLog,
  className,
  selected,
  updateSelected
}) => {
  const status = useStatus();

  const isConnected = useMemo(
    () => status.clientState.state === FILESPACE_CONNECTION_STATUSES.CONNECTED,
    [status.clientState.state]
  );

  return (
    <article className={classer([styles.Container, className])}>
      <h2 className={styles.CategoryName}>{categoryLog.category.label}</h2>
      <ul className={styles.List}>
        {categoryLog.metricLogs.map((metricLog) => {
          const checked = !!selected.find(
            (item) => item.id() === metricLog.id()
          );
          return (
            <li className={styles.ListItem} key={metricLog.metric.shortLabel}>
              <CheckChip
                className={styles.CheckChip}
                checked={checked}
                disabled={(!canAddMetric && !checked) || !isConnected}
                label={metricLog.metric.label}
                onChange={() => {
                  updateSelected(metricLog);
                }}
              />
              {}
            </li>
          );
        })}
      </ul>
    </article>
  );
};

export default memo(MetricCategory);
