import React, { memo } from 'react';

import { Paper } from '@components';
import { useMetrics } from '@providers';

import { MetricsList } from './components';

import styles from './MetricSelector.scss';

const MetricSelector = () => {
  const { canAddMetric, categories, selected, updateSelected } = useMetrics();

  return (
    <header className={styles.Container}>
      <Paper className={styles.Paper}>
        {categories.map((categoryLog) => {
          return (
            <MetricsList
              canAddMetric={canAddMetric}
              categoryLog={categoryLog}
              className={styles.MetricsList}
              key={categoryLog.category.name}
              selected={selected}
              updateSelected={updateSelected}
            />
          );
        })}
      </Paper>
    </header>
  );
};

export default memo(MetricSelector);
