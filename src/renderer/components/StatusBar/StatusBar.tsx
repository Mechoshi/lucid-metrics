import React, { memo } from 'react';

import { Loader, Paper, StatusIndicator } from '@components';
import { useStatus } from '@providers';

import styles from './StatusBar.scss';

const StatusBar = () => {
  const status = useStatus();

  return (
    <header className={styles.Container}>
      <Paper className={styles.Paper}>
        {!status.fileSystem.name ? (
          <Loader />
        ) : (
          <>
            <h1 className={styles.Filesystem}>
              <StatusIndicator status={status?.clientState?.state} />
              <p className={styles.NameGroup}>
                <span>Connected to: </span>
                <span className={styles.Name}>
                  {status.fileSystem ? status.fileSystem.name : ''}
                </span>
              </p>
            </h1>
          </>
        )}
      </Paper>
    </header>
  );
};

export default memo(StatusBar);
