import React, { FC, memo, useMemo } from 'react';
import { VictoryChart } from 'victory';

import { ChartLegend } from '@components';
import { Area, AxisX, AxisY } from '@components/MetricsChart/components';
import { useMockAxis } from '@components/MetricsChart/hooks';
import { ISnapshot } from '@providers';
import { getTimeAxisDomain, useParentDimensions } from '@services';

import styles from './SnapshotViewer.scss';

interface ISnapshotViewerProps {
  closeViewer: () => void;
  snapshot: ISnapshot;
}

const SnapshotViewer: FC<ISnapshotViewerProps> = ({
  closeViewer,
  snapshot
}) => {
  const mockAxis = useMockAxis();

  const domain = useMemo(
    () => getTimeAxisDomain(snapshot.date),
    [snapshot.date]
  );

  const { elementRef, height, width } = useParentDimensions<SVGSVGElement>(500);

  return (
    <div className={styles.Backdrop} onClick={closeViewer}>
      <aside className={styles.Container}>
        <div className={styles.Header}>
          <span className={styles.Filesystem}>{snapshot.filesystem}</span>
          {new Date(snapshot.date).toLocaleString()}
        </div>
        <ChartLegend metricLogs={snapshot.metricLogs} />
        <svg viewBox={`0 0 ${width} ${height}`} ref={elementRef}>
          <VictoryChart
            domain={domain}
            height={height}
            standalone={false}
            width={width}
          >
            {snapshot.metricLogs.map((metricLog, i) => {
              return (
                <Area
                  index={i}
                  key={metricLog.metric.label}
                  metricLog={metricLog}
                />
              );
            })}
            {snapshot.metricLogs.map((metricLog, index) => (
              <AxisY
                index={index}
                key={metricLog.metric.label}
                metricLog={metricLog}
              />
            ))}
            <AxisX />
            {mockAxis}
          </VictoryChart>
        </svg>
      </aside>
    </div>
  );
};

export default memo(SnapshotViewer);
