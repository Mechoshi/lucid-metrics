import React, { FC, memo, useCallback } from 'react';

import { Paper } from '@components';
import { ISnapshot } from '@providers';

import styles from './SnapshotCard.scss';

interface ISnapshotCardProps {
  open: (snapshot: ISnapshot) => void;
  removeSnapshot: (key: number) => Promise<void>;
  snapshot: ISnapshot;
}

const SnapshotCard: FC<ISnapshotCardProps> = ({
  open,
  removeSnapshot,
  snapshot
}) => {
  const remove = useCallback(() => {
    removeSnapshot(snapshot.date);
  }, [removeSnapshot, snapshot.date]);

  return (
    <li className={styles.Container}>
      <Paper className={styles.Paper}>
        <button
          className={styles.OpenSnapshotButton}
          onClick={() => {
            open(snapshot);
          }}
        >
          <div className={styles.Header}>
            <span className={styles.Filesystem}>{snapshot.filesystem}</span>
            {new Date(snapshot.date).toLocaleString()}
          </div>
          <ul className={styles.MetricLogs}>
            {snapshot.metricLogs.map((metricLog) => {
              return (
                <li className={styles.MetricLabel} key={metricLog.id()}>
                  {metricLog.categoryId} - {metricLog.metric.label}
                </li>
              );
            })}
          </ul>
        </button>
        <button className={styles.RemoveButton} onClick={remove}>
          Remove
        </button>
      </Paper>
    </li>
  );
};

export default memo(SnapshotCard);
