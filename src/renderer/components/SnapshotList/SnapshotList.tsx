import React, { FC, memo, useCallback, useState } from 'react';

import { Paper } from '@components';
import { ISnapshot, useSnapshot } from '@providers';

import { SnapshotCard, SnapshotViewer } from './components';

import styles from './SnapshotList.scss';

const SnapshotList: FC = () => {
  const { removeSnapshot, snapshots } = useSnapshot();
  const [selectedSnapshot, setSelectedSnapshot] = useState<ISnapshot | null>(
    null
  );

  const openViewer = useCallback((snapshot: ISnapshot) => {
    setSelectedSnapshot(snapshot);
  }, []);

  const closeViewer = useCallback(() => {
    setSelectedSnapshot(null);
  }, []);

  return snapshots.length ? (
    <section className={styles.Container}>
      {selectedSnapshot ? (
        <SnapshotViewer closeViewer={closeViewer} snapshot={selectedSnapshot} />
      ) : null}
      <Paper className={styles.Paper}>
        <ul className={styles.List}>
          {snapshots.map((snapshot) => {
            return (
              <SnapshotCard
                key={snapshot.date}
                open={openViewer}
                removeSnapshot={removeSnapshot}
                snapshot={snapshot}
              />
            );
          })}
        </ul>
      </Paper>
    </section>
  ) : null;
};

export default memo(SnapshotList);
