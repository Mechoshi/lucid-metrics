import React, { FC, memo, useCallback, useMemo } from 'react';
import { VictoryAxis, VictoryAxisProps } from 'victory';

import {
  COLORS,
  DATASET_COLORS,
  MetricLog,
  tickValuesGetters,
  Y_AXIS_OFFSETS
} from '@services';

import { TickLabelY } from '.';

interface IAxisYProps extends VictoryAxisProps {
  index: number;
  metricLog: MetricLog;
}

const AxisY: FC<IAxisYProps> = ({ index, metricLog, ...otherProps }) => {
  const tickFormat = useCallback(
    (metricValue) => {
      return metricValue * metricLog.getMaxima() || '';
    },
    [metricLog]
  );

  return (
    <VictoryAxis
      {...otherProps}
      dependentAxis
      offsetX={Y_AXIS_OFFSETS[index]}
      orientation={useMemo(() => {
        if (index === 0) {
          return 'left';
        } else if (index === 1) {
          return 'right';
        } else {
          return undefined;
        }
      }, [index])}
      style={useMemo(
        () => ({
          ...otherProps.style,
          axis: {
            stroke: DATASET_COLORS[index],
            stopOpacity: 0.8,
            strokeWidth: 1
          },
          grid: {
            stroke: COLORS.greyDark,
            strokeOpacity: 0.1,
            strokeWidth: 0.5
          },
          tickLabels: {
            padding: 1,
            fill: COLORS.greyLighter,
            fontSize: 10
          },
          ticks: { stroke: DATASET_COLORS[index] }
        }),
        [index, otherProps.style]
      )}
      tickFormat={tickFormat}
      tickLabelComponent={<TickLabelY metricLog={metricLog} />}
      tickValues={tickValuesGetters[metricLog.metric.type]()}
    />
  );
};

export default memo(AxisY);
