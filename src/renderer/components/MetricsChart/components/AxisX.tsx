import React, { memo, useMemo } from 'react';
import { VictoryAxis, VictoryAxisProps } from 'victory';

import { COLORS, formatTime, POLLING_PERIOD } from '@services';

const tickCount = Math.round(POLLING_PERIOD / 10000);

const AxisX = (props: VictoryAxisProps) => {
  return (
    <VictoryAxis
      {...props}
      style={useMemo(
        () => ({
          axis: {
            stroke: COLORS.greyLighter,
            strokeWidth: 0.5
          },
          tickLabels: { padding: 5, fill: COLORS.greyLighter, fontSize: 10 },
          ticks: { stroke: COLORS.grey }
        }),
        []
      )}
      tickCount={tickCount}
      tickFormat={formatTime}
    />
  );
};

export default memo(AxisX);
