import React, { FC, memo, useCallback, useMemo, useState } from 'react';
import { VictoryArea, VictoryAreaProps } from 'victory';

import {
  COLORS,
  DATASET_COLORS,
  labelGetters,
  MetricEntry,
  MetricLog
} from '@services';

interface IAreaProps extends VictoryAreaProps {
  index: number;
  metricLog: MetricLog;
}

const Area: FC<IAreaProps> = ({ index, metricLog, ...otherProps }) => {
  const [showTooltip, setShowTooltip] = useState(false);

  const { labelProp, labelStyles } = useMemo(() => {
    if (showTooltip) {
      return {
        labelProp: {
          labels: ({ datum }: { datum: MetricEntry }) =>
            labelGetters[metricLog.metric.type](datum.metricValue)
        },
        labelStyles: {
          labels: {
            padding: 3,
            fill: COLORS.greyLight,
            fontSize: 9,
            opacity: 0.6
          }
        }
      };
    } else {
      return {
        labelProp: {},
        labelStyles: {}
      };
    }
  }, [metricLog.metric.type, showTooltip]);

  const calculateY = useCallback(
    (metricEntry: MetricEntry) => {
      const maxima = metricLog.getMaxima();

      return maxima === 0 ? 0 : metricEntry.metricValue / maxima;
    },
    [metricLog]
  );

  return (
    <VictoryArea
      {...otherProps}
      animate={false}
      data={metricLog.data}
      interpolation="monotoneX"
      {...labelProp}
      style={useMemo(
        () => ({
          data: {
            stroke: DATASET_COLORS[index],
            strokeOpacity: 0.5,
            strokeWidth: 1,
            fillOpacity: 0.5,
            fill: DATASET_COLORS[index]
          },
          ...labelStyles
        }),
        [index, labelStyles]
      )}
      x="timestamp"
      y={calculateY}
      events={[
        {
          target: 'data',
          eventHandlers: {
            onMouseLeave: () => {
              setShowTooltip(false);
            },
            onMouseOver: () => {
              setShowTooltip(true);
            }
          }
        }
      ]}
    />
  );
};

export default memo(Area);
