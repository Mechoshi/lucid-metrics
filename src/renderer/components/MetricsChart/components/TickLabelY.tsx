import React, { FC, memo } from 'react';
import { VictoryLabel, VictoryLabelProps } from 'victory';

import { labelGetters, MetricLog } from '@services';

interface ITickLabelProps extends VictoryLabelProps {
  metricLog: MetricLog;
}

const TickLabelY: FC<ITickLabelProps> = ({
  datum,
  metricLog,
  text,
  ...otherProps
}) => {
  return (
    <VictoryLabel
      {...otherProps}
      // TODO: secure this:
      text={labelGetters[metricLog.metric.type](
        (text ? text : datum) as number
      )}
    />
  );
};

export default memo(TickLabelY);
