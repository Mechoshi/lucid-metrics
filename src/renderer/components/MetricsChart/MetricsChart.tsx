import React, { FC, memo } from 'react';
import { VictoryChart } from 'victory';

import { MetricLog, useParentDimensions } from '@services';

import { Area, AxisX, AxisY } from './components';
import { useDomain, useMockAxis } from './hooks';

type MetricsChartProps = {
  metricLogs: MetricLog[];
};

const MetricsChart: FC<MetricsChartProps> = ({ metricLogs }) => {
  const domain = useDomain();

  const mockAxis = useMockAxis();

  const { elementRef, height, width } = useParentDimensions<SVGSVGElement>();

  return (
    <svg ref={elementRef} viewBox={`0 0 ${width} ${height}`}>
      <VictoryChart
        domain={domain}
        height={height}
        standalone={false}
        width={width}
      >
        {metricLogs.map((metricLog, i) => {
          return (
            <Area
              index={i}
              key={metricLog.metric.label}
              metricLog={metricLog}
            />
          );
        })}
        {metricLogs.map((metricLog, index) => (
          <AxisY
            index={index}
            key={metricLog.metric.label}
            metricLog={metricLog}
          />
        ))}
        <AxisX />
        {mockAxis}
      </VictoryChart>
    </svg>
  );
};

export default memo(MetricsChart);
