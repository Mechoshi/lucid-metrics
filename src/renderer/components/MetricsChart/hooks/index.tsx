import React, { useEffect, useMemo, useState } from 'react';
import { DomainPropType, VictoryAxis } from 'victory';

import { getTimeAxisDomain, POLLING_INTERVAL } from '@services';

export const useDomain = (): DomainPropType => {
  const [domain, setDomain] = useState<DomainPropType>(getTimeAxisDomain());

  useEffect(() => {
    const domainShiftIntervalId = setInterval(() => {
      setDomain(getTimeAxisDomain());
    }, POLLING_INTERVAL);

    return () => {
      clearInterval(domainShiftIntervalId);
    };
  }, []);

  return domain;
};

// This tricks VictoryChart to hide its axis.
// This is because it can't 'see' the VictoryAxis
// components nested in custom components:
export const useMockAxis = (): JSX.Element => {
  return useMemo(() => {
    return (
      <VictoryAxis
        dependentAxis
        style={{
          axis: {
            display: 'none'
          },
          tickLabels: {
            display: 'none'
          }
        }}
      />
    );
  }, []);
};
