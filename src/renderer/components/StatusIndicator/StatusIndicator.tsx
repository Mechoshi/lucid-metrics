import React, { FC, memo } from 'react';

import { classer } from '@services';

import styles from './StatusIndicator.scss';

interface IStatusIndicatorProps {
  size?: 'medium';
  status: string;
}

const StatusIndicator: FC<IStatusIndicatorProps> = ({
  size = 'medium',
  status
}) => {
  return (
    <div
      className={classer([styles.Container, styles[status], styles[size]])}
    ></div>
  );
};

export default memo(StatusIndicator);
