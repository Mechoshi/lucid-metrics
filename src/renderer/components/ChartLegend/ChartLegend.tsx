import React, { FC, memo } from 'react';

import { DATASET_COLORS, MetricLog } from '@services';

import styles from './ChartLegend.scss';

interface IChartLegendProps {
  metricLogs: MetricLog[];
}

const ChartLegend: FC<IChartLegendProps> = ({ metricLogs }) => {
  return (
    <ul className={styles.Container}>
      {metricLogs.map((metricLog, index) => {
        return (
          <li className={styles.Metric} key={metricLog.metric.label}>
            <span
              className={styles.Indicator}
              style={{
                backgroundColor: DATASET_COLORS[index]
              }}
            />
            <span className={styles.Label}>{metricLog.metric.label}</span>
          </li>
        );
      })}
    </ul>
  );
};

export default memo(ChartLegend);
