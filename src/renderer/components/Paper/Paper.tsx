import { classer } from '@services';
import React, { FC, HTMLAttributes, memo, ReactNode } from 'react';

import styles from './Paper.scss';

interface IPaperProps {
  children: ReactNode;
  className?: HTMLAttributes<HTMLElement> | string;
}
const Paper: FC<IPaperProps> = ({ children, className }) => {
  return (
    <section className={classer([styles.Container, className])}>
      {children}
    </section>
  );
};

export default memo(Paper);
