import React, { memo, useCallback, useMemo } from 'react';

import {
  ChartLegend,
  Loader,
  MetricsChart,
  Paper,
  SnapshotList
} from '@components';
import { useMetrics, useSnapshot, useStatus } from '@providers';
import { FILESPACE_CONNECTION_STATUSES } from '@services';

import styles from './ChartSection.scss';

const ChartSection = () => {
  const { selected } = useMetrics();

  const { addSnapshot } = useSnapshot();

  const status = useStatus();

  const isConnected = useMemo(
    () => status.clientState.state === FILESPACE_CONNECTION_STATUSES.CONNECTED,
    [status.clientState.state]
  );

  const makeSnapshot = useCallback(() => {
    addSnapshot({
      date: new Date().getTime(),
      filesystem: status.fileSystem.name,
      value: JSON.stringify(selected)
    });
  }, [addSnapshot, selected, status.fileSystem.name]);

  return (
    <section className={styles.Container}>
      <Paper className={styles.Paper}>
        {isConnected ? (
          <>
            <ChartLegend metricLogs={selected} />
            <div className={styles.ChartContainer}>
              <MetricsChart metricLogs={selected} />{' '}
            </div>
            <button
              className={styles.MakeSnapshotButton}
              disabled={!selected.length}
              onClick={makeSnapshot}
            >
              Take Snapshot
            </button>
          </>
        ) : (
          <Loader width={300} />
        )}
      </Paper>
      <SnapshotList />
    </section>
  );
};

export default memo(ChartSection);
