declare module '*.scss' {
  const classes: { [key: string]: string };
  export default classes;
}

// eslint-disable-next-line @typescript-eslint/naming-convention
declare const __MODE__: string;

declare module 'react-ciao' {
  import { PureComponent, ReactNode } from 'react';
  interface ICiaoProps {
    enterClass: string;
    exitClass: string;
    exitDuration: number;
  }
  class Ciao extends PureComponent<
    ICiaoProps,
    { children: ReactNode; isExit: boolean }
  > {}

  export = Ciao;
}

declare module 'react-scroll-component' {
  import { CSSProperties, FC, PointerEventHandler } from 'react';

  export type ScrollDirection = 'horizontal' | 'vertical';

  export interface IScrollProps {
    className?: string;
    containerClass?: string;
    // eslint-disable-next-line @typescript-eslint/ban-types
    containerRef?: Function;
    dimensionChangeTimeout?: number;
    direction: ScrollDirection;
    display?: string;
    height?: string | number;
    initTimeout?: number;
    maxHeight?: string;
    maxWidth?: string;
    minScrollerSize?: number;
    noInitTimeout?: boolean;
    observe?: boolean;
    observerTimeout?: number;
    // eslint-disable-next-line @typescript-eslint/ban-types
    onScrollerToggle?: Function;
    onTrackClick?: PointerEventHandler;
    resizeDebounce?: number;
    scroller?: CSSProperties;
    scrollerClass?: string;
    scrollSizeDebounce?: number;
    track?: boolean;
    trackClass?: string;
    trackShift?: number;
    width?: string;
  }

  const Scroll: FC<IScrollProps>;
  export = Scroll;
}
