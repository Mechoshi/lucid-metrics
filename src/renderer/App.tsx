import React, { FC, memo } from 'react';

import { ChartSection, MetricSelector, StatusBar } from '@components';
import { MetricsProvider, SnapshotProvider, StatusProvider } from '@providers';

import styles from './App.scss';

const App: FC = () => {
  return (
    <SnapshotProvider>
      <StatusProvider>
        <StatusBar />
        <MetricsProvider>
          <main className={styles.Main}>
            <ChartSection />
            <MetricSelector />
          </main>
        </MetricsProvider>
      </StatusProvider>
    </SnapshotProvider>
  );
};

export default memo(App);
