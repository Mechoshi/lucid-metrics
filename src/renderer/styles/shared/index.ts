// eslint-disable-next-line @typescript-eslint/no-var-requires
const { resolve } = require('path');

const resources = ['_mixins.scss'];

module.exports = resources.map((file) => resolve(__dirname, file));
