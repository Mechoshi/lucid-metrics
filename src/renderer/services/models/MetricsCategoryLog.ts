import { fetchJson, MetricLog, MetricsCategory, MetricsData } from '@services';

export class MetricsCategoryLog {
  public category: MetricsCategory;

  private filespace: string;

  public metricLogs: MetricLog[] = [];

  private pollInterval: number;

  // eslint-disable-next-line no-undef
  private pollIntervalId: NodeJS.Timer | null = null;

  constructor(
    metricsCategory: MetricsCategory,
    pollInterval: number,
    filespace: string
  ) {
    this.category = metricsCategory;

    this.filespace = filespace;

    this.pollInterval = pollInterval;

    this.metricLogs = this.category.metrics.map((metric) => {
      return new MetricLog(
        metric,
        this.pollInterval,
        this.category.label,
        this.filespace
      );
    });
  }

  init(): void {
    this.poll();
  }

  fetchMetrics = async (): Promise<void> => {
    const metricsData = (await fetchJson(this.category.path)) as MetricsData;

    if (metricsData) {
      const timestamp = new Date().getTime();

      this.metricLogs.forEach((metricLog) => {
        metricLog.addEntry(metricsData, timestamp);
      });
    }
  };

  poll(): void {
    if (this.pollIntervalId) {
      clearInterval(this.pollIntervalId);
    }

    this.fetchMetrics();

    this.pollIntervalId = setInterval(this.fetchMetrics, this.pollInterval);
  }

  reset(): void {
    this.metricLogs.forEach((metricLog) => metricLog.reset());

    this.poll();
  }

  stop(): void {
    this.metricLogs.forEach((metricLog) => metricLog.reset());

    if (this.pollIntervalId) {
      console.log('Cleaning category interval');
      clearInterval(this.pollIntervalId);
    }
  }
}
