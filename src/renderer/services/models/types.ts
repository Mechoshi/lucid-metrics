export type MetricType = 'AverageTime' | 'DataSizePerSecond' | 'RatePerSecond';

export type Metric = {
  fields: string[];
  group: string;
  label: string;
  shortLabel: string;
  type: MetricType;
};

export type MetricsCategory = {
  label: string;
  metrics: Metric[];
  name: string;
  path: string;
};

export type MetricsSchema = MetricsCategory[];

export type MetricsData = {
  [key: string]: number | number[];
};

export type StatusData = {
  clientState: {
    state: string;
  };
  fileSystem: {
    name: string;
  };
  objectStoreState: string;
  serviceState: string;
};

export type MetricEntry = {
  metricValue: number;
  polledCount?: number;
  polledValue: number;
  timestamp: number;
};
