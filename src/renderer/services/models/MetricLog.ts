import {
  getMaximaPowerOfTen,
  Metric,
  metricCalculators,
  MetricEntry,
  MetricsData
} from '@services';

export interface ISnapshotValue {
  categoryId: string;
  data: MetricEntry[];
  filespace: string;
  metric: Metric;
  pollInterval: number;
}

export class MetricLog {
  categoryId: string;

  data: MetricEntry[] = [];

  filespace: string;

  metric: Metric;

  pollInterval: number;

  constructor(
    metric: Metric,
    pollInterval: number,
    categoryId: string,
    filespace: string
  ) {
    this.categoryId = categoryId;
    this.filespace = filespace;
    this.metric = metric;
    this.pollInterval = pollInterval;
  }

  static fromSnapshot({
    categoryId,
    filespace,
    data,
    metric,
    pollInterval
  }: ISnapshotValue): MetricLog {
    const metricLog = new MetricLog(
      metric,
      pollInterval,
      categoryId,
      filespace
    );
    metricLog.data = data;

    return metricLog;
  }

  addEntry(metricsData: MetricsData, timestamp: number): void {
    const newMetricEntry = metricCalculators[this.metric.type]({
      fields: this.metric.fields,
      lastMetricEntry: this.data[this.data.length - 1],
      metricsData,
      pollInterval: this.pollInterval,
      timestamp
    });

    this.data = [...this.data, newMetricEntry];

    const dataLength = this.data.length;

    if (dataLength > 62) {
      this.data = this.data.slice(dataLength - 62);
    }
  }

  // Used to normalize the Y values:
  // https://formidable.com/open-source/victory/gallery/multiple-dependent-axes
  getMaxima(): number {
    return getMaximaPowerOfTen(
      Math.max(...this.data.map((metricEntry) => metricEntry.metricValue))
    );
  }

  id(): string {
    return `${this.categoryId}-${this.metric.label}`;
  }

  reset(): void {
    this.data = [];
  }
}
