import { MetricEntry } from '@services';

// Keep in sync with 'styles/_variables.scss':
export const COLORS = {
  blueDark: '#5386c2',
  greenDark: '#9dc53b',
  greyLighter: '#f3f8ff',
  greyLight: '#bfbebf',
  grey: '#9d9c9e',
  greyDark: '#808184',
  greyDarker: '#2c2e2b',
  orange: '#f2951f'
};

export const DATASET_COLORS = [
  COLORS.greenDark,
  COLORS.orange,
  COLORS.blueDark
];

export const DEFAULT_METRIC_ENTRY: MetricEntry = {
  metricValue: 0,
  polledCount: 0,
  polledValue: 0,
  timestamp: 0
};

/**
 * Retrieved via http://localhost:7778/app/status
 *
 */
export const FILESPACE_CONNECTION_STATUSES = {
  CONNECTED: 'Linked' // ["clientState"]["state"] === "Linked"
};

export const MAX_DISPLAYED_METRICS = 3;

export const POLLING_INTERVAL = 1000; // milliseconds

export const POLLING_PERIOD = 60000; // milliseconds

export const SNAPSHOT_DB_NAME = 'Metrics-snapshots';

export const SNAPSHOT_OBJECT_STORE_NAME = 'snapshots';

export const Y_AXIS_OFFSETS = [undefined, undefined, 300];
