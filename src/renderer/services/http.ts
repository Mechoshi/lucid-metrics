import { MetricsData, LUCID_LINK_SERVER_URL, StatusData } from '@services';

export const fetchJson = async (
  path: string
): Promise<MetricsData | StatusData | null> => {
  try {
    const response = await fetch(`${LUCID_LINK_SERVER_URL}${path}`);
    const jsonData = await response.json();

    return jsonData;
  } catch (error) {
    // TODO: implement messaging:
    console.error(error);

    return null;
  }
};
