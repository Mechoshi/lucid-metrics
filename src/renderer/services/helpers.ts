import { HTMLAttributes } from 'react';
import { DomainPropType } from 'victory';

import {
  DEFAULT_METRIC_ENTRY,
  MetricEntry,
  MetricsData,
  POLLING_PERIOD
} from '@services';

type ClasserListItem =
  | string
  | null
  | boolean
  | undefined
  | HTMLAttributes<HTMLElement>;

export const classer = (classes: ClasserListItem[] = []): string => {
  return Array.isArray(classes) ? classes.filter(Boolean).join(' ') : classes;
};

export const formatBytes = (bytes: number): string => {
  if (bytes === 0) {
    return '';
  }

  if (bytes >= 1000000000) {
    return `${(bytes / 1000000000).toFixed(1)} GB`;
  }

  if (bytes >= 1000000) {
    return `${(bytes / 1000000).toFixed(1)} MB`;
  }

  if (bytes >= 1000) {
    return `${(bytes / 1000).toFixed(1)} KB`;
  }

  return `${bytes} B`;
};

export const formatMicroseconds = (microseconds: number): string => {
  if (microseconds === 0) {
    return '';
  }

  if (microseconds >= 1000000) {
    return `${(microseconds / 1000000).toFixed(1)} s`;
  }

  if (microseconds >= 1000) {
    return `${(microseconds / 1000).toFixed(1)} ms`;
  }

  return `${microseconds.toFixed(1)} mks`;
};

export const formatTime = (timestamp: number): string =>
  new Date(timestamp).toLocaleTimeString();

export const getTimeAxisDomain = (now?: number): DomainPropType => {
  const nowTimestamp = now || new Date().getTime() - 1000;

  const pollingHorizon = nowTimestamp - POLLING_PERIOD;

  return { x: [pollingHorizon, nowTimestamp], y: [0, 1] };
};

export const getMaximaPowerOfTen = (maxima: number): number => {
  let currentPower = 0;
  let currentMaxima = maxima;

  while (currentMaxima > 1) {
    currentMaxima /= 10;
    currentPower++;
  }

  return 10 ** currentPower;
};

export const metricCalculators = {
  AverageTime: ({
    fields,
    lastMetricEntry = DEFAULT_METRIC_ENTRY,
    metricsData,
    timestamp
  }: {
    fields: string[];
    lastMetricEntry: MetricEntry;
    metricsData: MetricsData;
    pollInterval?: number;
    timestamp: number;
  }): MetricEntry => {
    const timeKey = fields[0];
    const countKey = fields[1];

    const lastPolledCount = lastMetricEntry.polledCount || 0;
    const lastPolledTime = lastMetricEntry.polledValue;

    // TODO: make this safer:
    const newPolledCount = metricsData[countKey] as number;
    const newPolledTime = metricsData[timeKey] as number;

    const countChange = newPolledCount - lastPolledCount;
    const timeChange = newPolledTime - lastPolledTime;

    const metricValue = isNaN(timeChange / countChange)
      ? 0
      : Math.round(timeChange / countChange);

    return {
      metricValue,
      polledCount: newPolledCount,
      polledValue: newPolledTime,
      timestamp
    };
  },
  DataSizePerSecond: ({
    fields,
    lastMetricEntry = DEFAULT_METRIC_ENTRY,
    metricsData,
    pollInterval,
    timestamp
  }: {
    fields: string[];
    lastMetricEntry: MetricEntry;
    metricsData: MetricsData;
    pollInterval: number;
    timestamp: number;
  }): MetricEntry => {
    const valueKey = fields[0];

    const lastPolledValue = lastMetricEntry.polledValue;

    // TODO: make this safer:
    const newPolledValue = metricsData[valueKey] as number;

    return {
      metricValue:
        lastPolledValue === 0
          ? 0
          : Math.round(
              (newPolledValue - lastPolledValue) / (pollInterval / 1000)
            ),
      polledValue: newPolledValue,
      timestamp
    };
  },
  RatePerSecond: ({
    fields,
    lastMetricEntry = DEFAULT_METRIC_ENTRY,
    metricsData,
    pollInterval,
    timestamp
  }: {
    fields: string[];
    lastMetricEntry: MetricEntry;
    metricsData: MetricsData;
    pollInterval: number;
    timestamp: number;
  }): MetricEntry => {
    const valueKey = fields[0];

    const lastPolledValue = lastMetricEntry.polledValue;

    // TODO: make this safer:
    const newPolledValue = metricsData[valueKey] as number;

    return {
      metricValue:
        lastPolledValue === 0
          ? 0
          : (newPolledValue - lastPolledValue) / pollInterval,
      polledValue: newPolledValue,
      timestamp
    };
  }
};

export const labelGetters = {
  AverageTime: (microseconds: number): string =>
    formatMicroseconds(microseconds),
  DataSizePerSecond: (bytes: number): string =>
    bytes > 0 ? `${formatBytes(bytes)}/s` : '',
  RatePerSecond: (rate: number): string => (rate > 0 ? `${rate}/s` : '')
};

export const tickValuesGetters = {
  AverageTime: (): number[] => [0.25, 0.5, 0.75, 1],
  DataSizePerSecond: (): number[] => [0.25, 0.5, 0.75, 1],
  RatePerSecond: (): number[] => [0, 1]
};
