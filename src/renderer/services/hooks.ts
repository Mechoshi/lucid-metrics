import { RefObject, useEffect, useRef, useState } from 'react';

interface IUseParentDirectionReturnValue<
  T extends HTMLElement | SVGSVGElement
> {
  elementRef: RefObject<T>;
  height: number;
  width: number;
}

export function useParentDimensions<T extends HTMLElement | SVGSVGElement>(
  updateTimeout?: number
): IUseParentDirectionReturnValue<T> {
  const [, setState] = useState({});

  useEffect(() => {
    let timeoutId: number;

    const update = () => setState({});

    if (updateTimeout) {
      timeoutId = setTimeout(update, updateTimeout) as unknown as number;
    }

    window.addEventListener('resize', update);

    return () => {
      if (timeoutId) {
        clearTimeout(timeoutId);
      }

      window.removeEventListener('resize', update);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const elementRef = useRef<T>(null);

  const width = elementRef.current?.parentElement?.offsetWidth || 0;

  const height = elementRef.current?.parentElement?.offsetHeight || 0;

  return {
    elementRef,
    height,
    width
  };
}
