export const LUCID_LINK_API_PATHS = {
  STATUS: 'app/status'
};

export const LUCID_LINK_SERVER_URL = 'http://localhost:7778/';
