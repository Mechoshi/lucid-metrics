import React, {
  createContext,
  FC,
  memo,
  useCallback,
  useContext,
  useEffect,
  useState
} from 'react';

import {
  fetchJson,
  LUCID_LINK_API_PATHS,
  POLLING_INTERVAL,
  StatusData
} from '@services';

const initialState: StatusData = {
  clientState: {
    state: ''
  },
  fileSystem: {
    name: ''
  },
  objectStoreState: '',
  serviceState: ''
};

const StatusContext = createContext<StatusData>(initialState);

export const useStatus = (): StatusData => {
  return useContext(StatusContext);
};

export const StatusProvider: FC = memo(({ children }) => {
  const [status, setStatus] = useState<StatusData>(initialState);

  const fetchStatus = useCallback(() => {
    // TODO: Think of a better type
    fetchJson(LUCID_LINK_API_PATHS.STATUS).then((data) =>
      data
        ? setStatus({ ...initialState, ...(data as StatusData) })
        : setStatus(initialState)
    );
  }, []);

  useEffect(() => {
    fetchStatus();
    const statusPollIntervalId = setInterval(fetchStatus, POLLING_INTERVAL);

    return () => {
      clearInterval(statusPollIntervalId);
    };
  }, [fetchStatus]);

  return (
    <StatusContext.Provider value={status}>{children}</StatusContext.Provider>
  );
});
