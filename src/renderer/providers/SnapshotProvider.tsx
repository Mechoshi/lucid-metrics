import React, {
  createContext,
  FC,
  memo,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState
} from 'react';
import { openDB } from 'idb';

import {
  ISnapshotValue,
  MetricLog,
  SNAPSHOT_DB_NAME,
  SNAPSHOT_OBJECT_STORE_NAME
} from '@services';

const dbPromise = openDB(SNAPSHOT_DB_NAME, 1, {
  upgrade(db) {
    const store = db.createObjectStore(SNAPSHOT_OBJECT_STORE_NAME, {
      keyPath: 'date'
    });

    store.createIndex('date', 'date');
  }
});

const getAll = async (): Promise<ISnapshotDBEntry[]> => {
  return (await dbPromise).getAllFromIndex(SNAPSHOT_OBJECT_STORE_NAME, 'date');
};

const add = async (val: ISnapshotDBEntry) => {
  return (await dbPromise).add(SNAPSHOT_OBJECT_STORE_NAME, val);
};

const del = async (key: number) => {
  return (await dbPromise).delete(SNAPSHOT_OBJECT_STORE_NAME, key);
};

export interface ISnapshotDBEntry {
  date: number;
  filesystem: string;
  value: string;
}

export interface ISnapshot {
  date: number;
  filesystem: string;
  metricLogs: MetricLog[];
}

export interface ISnapshotProviderContext {
  addSnapshot: (value: ISnapshotDBEntry) => Promise<void>;
  removeSnapshot: (key: number) => Promise<void>;
  snapshots: ISnapshot[];
}

const initialContext: ISnapshotProviderContext = {
  addSnapshot: async () => {},
  removeSnapshot: async () => {},
  snapshots: []
};

const SnapshotContext = createContext<ISnapshotProviderContext>(initialContext);

export const useSnapshot = (): ISnapshotProviderContext => {
  return useContext(SnapshotContext);
};

export const SnapshotProvider: FC = memo(({ children }) => {
  const [snapshots, setSnapshots] = useState<ISnapshot[]>([]);

  const updateSnapshots = useCallback(async () => {
    try {
      const snapshotsFromDB = await getAll();

      const normalizedSnapshots: ISnapshot[] = snapshotsFromDB.map((item) => {
        const metricLogs = JSON.parse(item.value).map(
          (value: ISnapshotValue) => {
            return MetricLog.fromSnapshot(value);
          }
        );

        return {
          date: item.date,
          filesystem: item.filesystem,
          metricLogs
        };
      });

      normalizedSnapshots.reverse();

      setSnapshots(normalizedSnapshots);
    } catch (error) {
      console.error(error);
    }
  }, []);

  const addSnapshot = useCallback(
    async (value: ISnapshotDBEntry) => {
      try {
        await add(value);

        await updateSnapshots();
      } catch (error) {
        console.error(error);
      }
    },
    [updateSnapshots]
  );

  const removeSnapshot = useCallback(
    async (key: number) => {
      try {
        await del(key);

        await updateSnapshots();
      } catch (error) {
        console.error(error);
      }
    },
    [updateSnapshots]
  );

  const context = useMemo<ISnapshotProviderContext>(() => {
    return {
      addSnapshot,
      removeSnapshot,
      snapshots
    };
  }, [addSnapshot, removeSnapshot, snapshots]);

  useEffect(() => {
    updateSnapshots();
  }, [updateSnapshots]);

  return (
    <SnapshotContext.Provider value={context}>
      {children}
    </SnapshotContext.Provider>
  );
});
