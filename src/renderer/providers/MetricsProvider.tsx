import React, {
  createContext,
  FC,
  memo,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useReducer
} from 'react';

import { useStatus } from '@providers';

import {
  FILESPACE_CONNECTION_STATUSES,
  MAX_DISPLAYED_METRICS,
  MetricLog,
  MetricsCategoryLog,
  METRICS_SCHEMA,
  POLLING_INTERVAL
} from '@services';

interface MetricsProviderState {
  canAddMetric: boolean;
  categories: MetricsCategoryLog[];
  selected: MetricLog[];
}

const initialState: MetricsProviderState = {
  canAddMetric: true,
  categories: [],
  selected: []
};

const initialContext: IMetricsProviderContext = {
  ...initialState,
  updateSelected: () => {}
};

interface IMetricsProviderAction {
  categoryLogs?: MetricsCategoryLog[];
  metricLog?: MetricLog;
  type: 'CATEGORIES_CLEAR' | 'CATEGORIES_SET' | 'SELECTED_UPDATE';
}

const reducer = (
  state: MetricsProviderState,
  action: IMetricsProviderAction
) => {
  switch (action.type) {
    case 'CATEGORIES_CLEAR':
      return {
        ...state,
        canAddMetric: true,
        categories: [],
        selected: []
      };

    case 'CATEGORIES_SET':
      return {
        ...state,
        categories: action.categoryLogs || []
      };

    case 'SELECTED_UPDATE':
      if (!(action.metricLog instanceof MetricLog)) {
        return state;
      }
      // eslint-disable-next-line no-case-declarations
      const exists = !!state.selected.find(
        (metricLog) => metricLog.id() === action.metricLog?.id()
      );

      // eslint-disable-next-line no-case-declarations
      const updatedSelected = exists
        ? state.selected.filter(
            (metricLog) => metricLog.id() !== action.metricLog?.id()
          )
        : [...state.selected, action.metricLog];

      return {
        ...state,
        canAddMetric: updatedSelected.length < MAX_DISPLAYED_METRICS,
        selected: updatedSelected
      };
    default:
      return state;
  }
};

interface IMetricsProviderContext extends MetricsProviderState {
  updateSelected: (metricLog: MetricLog) => void;
}

const MetricsContext = createContext<IMetricsProviderContext>(initialContext);

export const useMetrics = (): IMetricsProviderContext => {
  return useContext(MetricsContext);
};

export const MetricsProvider: FC = memo(({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const status = useStatus();

  const isConnected = useMemo(
    () => status.clientState.state === FILESPACE_CONNECTION_STATUSES.CONNECTED,
    [status.clientState.state]
  );

  useEffect(() => {
    if (!isConnected) {
      if (state.categories.length > 0) {
        state.categories.forEach((metricsCategoryLog) =>
          metricsCategoryLog.stop()
        );
      }
    }
  }, [isConnected, state.categories]);

  useEffect(() => {
    if (state.categories.length > 0) {
      state.categories.forEach((metricsCategoryLog) =>
        metricsCategoryLog.stop()
      );

      dispatch({ type: 'CATEGORIES_CLEAR' });
    }

    if (status.fileSystem.name) {
      const newMetricsCategoryLogs = METRICS_SCHEMA.map((metricsCategory) => {
        return new MetricsCategoryLog(
          metricsCategory,
          POLLING_INTERVAL,
          status.fileSystem.name
        );
      });

      // Start polling:
      newMetricsCategoryLogs.forEach((metricsCategoryLog) =>
        metricsCategoryLog.init()
      );

      dispatch({
        categoryLogs: newMetricsCategoryLogs,
        type: 'CATEGORIES_SET'
      });
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, status.fileSystem.name]);

  const updateSelected = useCallback(
    (metricLog: MetricLog) => {
      dispatch({ type: 'SELECTED_UPDATE', metricLog });
    },
    [dispatch]
  );

  const contextState = useMemo<IMetricsProviderContext>(() => {
    return {
      ...state,
      updateSelected
    };
  }, [state, updateSelected]);

  return (
    <MetricsContext.Provider value={contextState}>
      {children}
    </MetricsContext.Provider>
  );
});
